class Rsc_FRL_RadioHint
{
	idd = -1;
	onLoad = "uiNamespace setVariable ['frl_RadioHintDisplay', _this select 0]";
	onUnload = "uiNamespace setVariable ['frl_RadioHintDisplay', displayNull]";
	fadeIn=0;
	fadeOut=0.2;
	movingEnable = false;
	duration = 10e10;
	name = "Rsc_FRL_RadioHint";
	class controls {
		class InformationText: RscStructuredText {
			idc = 1;
			text = "";

			/* -- Below chat in bottom left corner -- */
			x = "(profilenamespace getvariable [""IGUI_GRID_CHAT_X"",(safezoneX + 1 * (((safezoneW / safezoneH) min 1.2) / 40))])";
			y = "(profilenamespace getvariable [""IGUI_GRID_CHAT_Y"",(safezoneY + safezoneH - 10.5 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25))]) + (profilenamespace getvariable [""IGUI_GRID_CHAT_H"", (5 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25))])";
			w = "(10 * (((safezoneW / safezoneH) min 1.2) / 40))";
			h = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";

			colorText[] = {1,1,1,1};
			colorBackground[] = {0.1,0.1,0.1,0.5};
			sizeEx = 1;
			size = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
		};
	};
};