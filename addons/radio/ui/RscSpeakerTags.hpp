class Rsc_FRL_RadioSpeaker
{
	idd = -1;
	onLoad = "uiNamespace setVariable ['frl_RadioSpeakerDisplay', _this select 0]";
	onUnload = "uiNamespace setVariable ['frl_RadioSpeakerDisplay', displayNull]";
	fadeIn=0;
	fadeOut=0;
	movingEnable = false;
	duration = 10e10;
	name = "Rsc_FRL_RadioSpeaker";
	class controls {
		class SpeakerTags: RscStructuredText {
			idc = 1;
			text = "";
			colorBackground[] = {0,0,0,0};
			shadow = 1;
			shadowColor[] = {0,0,0,0.5};
			font = "RobotoCondensed";

			/* -- Below chat (In normal position of speaker tags) -- */
			// x = "(profilenamespace getvariable [""IGUI_GRID_CHAT_X"",(safezoneX + 1 * (((safezoneW / safezoneH) min 1.2) / 40))])";
			// y = "(profilenamespace getvariable [""IGUI_GRID_CHAT_Y"",(safezoneY + safezoneH - 10.5 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25))]) + (profilenamespace getvariable [""IGUI_GRID_CHAT_H"", (5 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25))])";
			// w = "(profilenamespace getvariable [""IGUI_GRID_CHAT_W"",(20 * (((safezoneW / safezoneH) min 1.2) / 40))])";
			// h = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1.6)";

			/* -- Bottom right corner -- */
			// x = "safeZoneX + SafeZoneW - (20 * (((safezoneW / safezoneH) min 1.2) / 40))";
			// y = "safeZoneY + SafeZoneH - (((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
			// w = "(20 * (((safezoneW / safezoneH) min 1.2) / 40))";
			// h = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";

			/* -- right middle -- */
			x = "(safeZoneX + SafeZoneW - (10 * (((safezoneW / safezoneH) min 1.2) / 40)))";
			y = "(safeZoneY + (SafeZoneH * (0.5 - 0.08)))";
			w = "(10 * (((safezoneW / safezoneH) min 1.2) / 40))";
			h = "(safeZoneH * 0.16)";

			colorText[] = {1,1,1,1};
			sizeEx = 1;
			size = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1.2)";
			class Attributes: Attributes {
				shadow = 1;
				align = "right";
			};
		};
	};
};