/*
    Function:       FRL_Radio_fnc_addPerFrameHandler
    Author:         Adanteh
    Description:    Wrapper for adding per frame handler (Clib, CBA)
    Example:        [player] call FRL_Radio_fnc_addPerFrameHandler;
*/

if !(isNil "Clib_fnc_addPerFrameHandler") exitWith {
    (_this call Clib_fnc_addPerFrameHandler);
};

if !(isNil "CBA_fnc_addPerFrameHandler") exitWith {
    params [["_function", {}, [{}]], ["_delay", 0, [0]], ["_args", []]];
    ([_function, _args, _delay] call CBA_fnc_addPerFrameHandler);
};

-1
