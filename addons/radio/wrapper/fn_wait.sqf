/*
    Function:       FRL_Radio_fnc_wait
    Author:         Adanteh
    Description:    Wrapper for unscheduled wait function (Clib, CBA)
    Example:        [player] call FRL_Radio_fnc_wait;
*/

params [["_code", {}], ["_time", 0], ["_args", []]];
if !(isNil "Clib_fnc_wait") exitWith {
    (_this call Clib_fnc_wait);
};

if !(isNil "CBA_fnc_waitAndExecute") exitWith {
    ([_code, _args, _time] call CBA_fnc_waitAndExecute);
};

-1
