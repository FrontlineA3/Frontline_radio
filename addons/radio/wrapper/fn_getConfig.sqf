/*
    Function:       FRL_Radio_fnc_getConfig
    Author:         Adanteh
    Description:    Wrapper for config setting retreival
    Example:        ["channelName"] call FRL_Radio_fnc_getConfig;
*/

params [["_value",  "", [""]], "_defaultValue"];
if !(isNil "FRL_Main_fnc_getExternalConfig") exitWith {
    (_this call FRL_Main_fnc_getExternalConfig);
};

_defaultValue;
