/*
    Function:       FRL_Radio_fnc_keybindCheck
    Author:         Adanteh
    Description:    Checks if keybind used properly
    Example:        [player] call FRL_Radio_fnc_keybindCheck;
*/
#include "macros.hpp"

params ["_keydown", "_args"];
_args params ["", "_key", "_shift", "_ctrl", "_alt"];

private _return = false;
private _index = 0;
{
	// -- This allows us to use nil modifier, which ignores it -- //
	(_x select 1) params [["_shiftCheck", _shift], ["_ctrlCheck", _ctrl], ["_altCheck", _alt]];
	if (_keydown) then {
		if (([_x select 0, [_shiftCheck, _ctrlCheck, _altCheck]]) isEqualTo [_key, [_shift, _ctrl, _alt]]) then {
			_return = call (((GVAR(keybindArray) select 1) select _index) select 0);
		};
	} else {
		// -- Sometimes we don't want to check if the same modifiers are still held when releasing key. -- //
		if ((GVAR(keybindArray) select 2) select _index) then {
			if (([_x select 0, [_shiftCheck, _ctrlCheck, _altCheck]]) isEqualTo [_key, [_shift, _ctrl, _alt]]) then {
				_return = call (((GVAR(keybindArray) select 1) select _index) select 1);
			};
		} else {
			if ((_x select 0) isEqualTo _key) then {
				_return = call (((GVAR(keybindArray) select 1) select _index) select 1);
			};
		};
	};
	if (_return) exitWith { };
    _index = _index + 1;
	nil;
} count (GVAR(keybindArray) select 0);

_return
