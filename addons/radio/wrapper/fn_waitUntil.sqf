/*
    Function:       FRL_Radio_fnc_waitUntil
    Author:         Adanteh
    Description:    Wrapper for unscheduled waitUntil function (Clib, CBA)
    Example:        [player] call FRL_Radio_fnc_waitUntil;
*/

params [["_code", {}], ["_time", 0], ["_args", []]];
if !(isNil "Clib_fnc_waitUntil") exitWith {
    (_this call Clib_fnc_waitUntil);
};

if !(isNil "CBA_fnc_waitUntilAndExecute") exitWith {
    ([_code, _args, _time] call CBA_fnc_waitUntilAndExecute);
};

-1
