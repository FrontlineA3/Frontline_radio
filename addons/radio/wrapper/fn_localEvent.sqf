/*
    Function:       FRL_Radio_fnc_localEvent
    Author:         Adanteh
    Description:    Wrapper for adding keybind to whichever keybind system is loaded
    Example:        [player] call FRL_Radio_fnc_localEvent;
*/

if !(isNil "Clib_fnc_localEvent") exitWith {
    (_this call Clib_fnc_localEvent);
};

if !(isNil "CBA_fnc_localEvent") exitWith {
    (_this call CBA_fnc_localEvent);
};

-1
