/*
    Function:       FRL_Radio_fnc_addEventHandler
    Author:         Adanteh
    Description:    Wrapper for adding event (Clib, CBA)
    Example:        [player] call FRL_Radio_fnc_addEventHandler;
*/

if !(isNil "Clib_fnc_addEventHandler") exitWith {
    (_this call Clib_fnc_addEventHandler);
};

if !(isNil "CBA_fnc_addEventHandlerArgs") exitWith {
    params [["_event", "", [""]], ["_function", {}, [{}, ""]], ["_args", []]];
    if (_args isEqualTo []) then {
        ([_event, _function] call CBA_fnc_addEventHandler);
    } else {
        ([_event, _function, _args] call CBA_fnc_addEventHandlerArgs);
    };
};

-1
