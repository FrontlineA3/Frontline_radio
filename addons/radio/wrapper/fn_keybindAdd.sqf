/*
    Function:       FRL_Radio_fnc_keybindAdd
    Author:         Adanteh
    Description:    Wrapper for adding keybind to whichever keybind system is loaded
    Example:        [player] call FRL_Radio_fnc_keybindAdd;
*/
#include "macros.hpp"


if (isNil "FRL_Main_fnc_addKeybind") then {
    params ["_category", "_tag", "_tagName", "_downCode", "_upCode", "_keybind", "_something", ["_modifierRelease", true]];
    if (isNil QGVAR(keybindArray)) then {
        GVAR(keybindArray) = [[], [], []];
        [] spawn {
            waitUntil { !isNull (findDisplay 46) };
            ((findDisplay 46) displayAddEventHandler ["keyDown", (format ["[true, _this] call %1;", QFUNC(keybindCheck)])]);
            ((findDisplay 46) displayAddEventHandler ["keyUp", (format ["[false, _this] call %1;", QFUNC(keybindCheck)])]);
        };
    };

    _keybindIndex = (GVAR(keybindArray) select 0) pushBack _keybind;

    if (_downCode isEqualType "") then { _downCode = compile _downCode; };
    if (_downCode isEqualTo {}) then { _downCode = { false }; };
    if (_upCode isEqualType "") then { _upCode = compile _upCode; };
    if (_upCode isEqualTo {}) then { _downCode = { false }; };

    (GVAR(keybindArray) select 1) set [_keybindIndex, [_downCode, _upCode]];
    (GVAR(keybindArray) select 2) set [_keybindIndex, _modifierRelease];

    _keybindIndex;
} else {
    _this call FRL_Main_fnc_addKeybind;
};
