/*
    Function:       FRL_Radio_fnc_name
    Author:         Adanteh
    Description:    Wrapper for name (Because A3 still doesn't support names on dead units) (Clib)
    Example:        [player] call FRL_Radio_fnc_name;
*/
#include "macros.hpp"

if !(isNil "Clib_fnc_name") exitWith {
    _this call Clib_fnc_name;
};

params ["_unit"];
if (alive _unit) then {
    private _name = name _unit;
    _unit setVariable [QGVAR(name), _name];
    _name
} else {
    (_unit getVariable [QGVAR(name), name _unit]);
};
