/*
    Function:       FRL_Radio_fnc_getChannelFrequency
    Author:         Adanteh
    Description:    Gets TFAR-style frequency for given channel
	Arguments:
		0: The channel ID <SCALAR>
    	1: The unit to get frequency for <OBJECT>
	Example:		[currentChannel, player] call FRL_Radio_fnc_getChannelFrequency
*/
#include "macros.hpp"

params [["_channel", currentChannel], ["_unit", player, [objNull, grpNull]]];

private _frequency = switch (_channel) do {
	case _CH_SIDE: 		{ missionNamespace getVariable (format ["FRL_rSid%1", ([side group _unit] call BIS_fnc_sideID)]); };
	case _CH_COMMAND: 	{ missionNamespace getVariable (format ["FRL_rCom%1", ([side group _unit] call BIS_fnc_sideID)]); };
	case _CH_GROUP: 	{ if (_unit isEqualType objNull) then { _unit = group _unit; }; (netID _unit) };
	case _CH_VEHICLE: 	{ netID (vehicle _unit) };
	default { "1"; };
};

_frequency;
