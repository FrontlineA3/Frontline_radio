/*
 	Name: TFAR_fnc_vehicleID

 	Author(s):
		NKey

 	Description:
		Returns a string with information about the player vehicle, used at the plugin side.

 	Parameters:
		0: OBJECT - The unit to check.

 	Returns:
		STRING - NetworkID, Turned out

 	Example:
		_vehicleID = player call FUNC(vehicleID);
*/
#include "macros.hpp"

VAR(_result) = "no";
if ((vehicle _this) != _this) then {
	_result = netid (vehicle _this);
	if (_result == "") then {
		_result = "singleplayer";
	};
	if (isTurnedOut _this) then {
		_result = _result + "_turnout";
	} else {
		_result = _result + "_0";
	};
};
_result
