/*
    Function:       FRL_Radio_fnc_canSpeak
    Author:         Adanteh
    Description:    Controls whether you can use the radio/direct comms
    Example:        [player] call FRL_Radio_fnc_canSpeak;
*/

#include "macros.hpp"
true
