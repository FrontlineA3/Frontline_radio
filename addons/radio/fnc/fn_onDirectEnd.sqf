/*
 *	File: fn_onDirectEnd.sqf
 *	Author: Adanteh
 *	Hides the broadcasting hint when you're talking in local
 *
 *	0: Unit talking on direct <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[] call FUNC(onDirectEnd);
 */

#include "macros.hpp"

(_this select 0) params ["_unit", "_nearby", "_name"];

if (_unit isEqualTo TFAR_currentUnit) then {
	if !(GVAR(lastError)) then {
		[] call FUNC(radioHintHide);
	};
};

true;