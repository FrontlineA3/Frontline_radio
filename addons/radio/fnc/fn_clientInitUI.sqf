/*
    Function:       FRL_Radio_fnc_clientInitUI
    Author:         Adanteh
    Description:    Handles the showing of radio-speaking tags (Right side of screen)
*/

#include "macros.hpp"

// -- Process the config values (Which have their values multiplied by 1.4 because BIS) -- //
#define __PARSE apply { (if (_x isEqualType "") then { ((call compile _x) / 1.4) } else { _x }) }
#define __RADIOIMG "<img image='\A3\ui_f\data\igui\rscingameui\rscdisplayvoicechat\microphone_ca.paa'/>"
#define ctrl(x) (uiNamespace getVariable QSVAR(RadioSpeakerDisplay) displayCtrl x)

// -- Retrieve channel colors from configs -- //
VAR(_chatConfig) = configFile >> "RscChatListDefault";
VAR(_colorCommand) = (getArray (_chatConfig >> "colorCommandChannel")) __PARSE;
VAR(_colorGroup) = (getArray (_chatConfig >> "colorGroupChannel")) __PARSE;
VAR(_colorVehicle) = (getArray (_chatConfig >> "colorVehicleChannel")) __PARSE;
VAR(_colorDirect) = (getArray (_chatConfig >> "colorDirectChannel")) __PARSE;
VAR(_colorSide) = (getArray (_chatConfig >> "colorSideChannel")) __PARSE;

VAR(_colorGroupHTML) = _colorGroup call BIS_fnc_colorRGBAtoHTML;
VAR(_colorDirectHTML) = _colorDirect call BIS_fnc_colorRGBAtoHTML;
VAR(_colorVehicleHTML) = _colorVehicle call BIS_fnc_colorRGBAtoHTML;
VAR(_colorSideHTML) = _colorSide call BIS_fnc_colorRGBAtoHTML;
VAR(_colorCommandHTML) = _colorCommand call BIS_fnc_colorRGBAtoHTML;

GVAR(speakerTags) = [];
GVAR(channelColors) = ["#FFFFFFFF", _colorSideHTML, _colorCommandHTML, _colorGroupHTML, _colorVehicleHTML, _colorDirectHTML];

["fadeHud", {
	(_this select 0) params ["_fade", "_commit"];
	// -- CtrlSetFade doesn't work on map controls, it only supports 0 or 1
	if (isNull (uiNamespace getVariable [QSVAR(RadioHintDisplay), displayNull])) exitWith { };
	((uiNamespace getVariable QSVAR(RadioHintDisplay)) displayCtrl 1) ctrlSetFade _fade;
	((uiNamespace getVariable QSVAR(RadioHintDisplay)) displayCtrl 1) ctrlCommit _commit;
}] call FUNC(addEventHandler);

// -- handle to create formatted text with that are currently talking -- //
[{
	if (count GVAR(speakerTags) > 0) then {

		VAR(_speakerText) = "";
		{
			_x params ["_name", "_color", "_img", "_unit"];

			// -- Check if unit is still speaking, if not delete from array -- //
			if ([_unit] call FUNC(unitIsSpeaking)) then {
				_speakerText = _speakerText + format ["<t color='%3'>%2 %1</t><br/>",
					(_x select 0),
					_img,
					_color];
			} else {
				GVAR(speakerTags) deleteAt (GVAR(speakerTags) find _x);
			};

			// -- Max rows -- //
			if (_forEachIndex == 10) exitWith { };
		} forEach GVAR(speakerTags);

		// -- If not shown yet, show now -- //
		if (isNull(uiNamespace getVariable [QSVAR(RadioSpeakerDisplay), displayNull])) then {
			QGVAR(nametagsLayer) cutRsc ["Rsc_FRL_RadioSpeaker", "PLAIN", 0, true];
		};

		ctrl(1) ctrlSetStructuredText parseText _speakerText;
	} else {
		QGVAR(nametagsLayer) cutText ["", "PLAIN"];
	};

}, 0, []] call FUNC(addPerFrameHandler);

// -- Add unit to talking array -- //

["talkStart", {
	(_this select 0) params ["_unit", "_nearby", "_name"];
	if !(_unit isEqualTo player) then {
		if ((side (group _unit)) isEqualTo playerSide) then {

			// -- Determine which color and image to use for tag -- //
			// -- @todo: Figure out reliable way to determine which channel someone is talking on (Pls no broadcast) -- //
			VAR(_img) = ([__RADIOIMG, ""] select _nearby);
			VAR(_channel) = switch (true) do {
				case ((vehicle _unit) isEqualTo (vehicle player)): 	{ _CH_VEHICLE };
				case ((group _unit) isEqualTo (group player)): 		{ _CH_GROUP };
				case ((leader _unit) isEqualTo _unit):				{ _CH_COMMAND };
				case (_nearby): 									{ _CH_DIRECT };
				default { -1 };
			};

			if (_channel != -1) then {
				VAR(_color) = [_channel] call FUNC(getChannelColor);
				GVAR(speakerTags) pushBackUnique [_name, _color, _img, _unit];
			};
		};
	};
}] call FUNC(addEventHandler);

["talkStart", FUNC(onDirectStart)] call FUNC(addEventHandler);
["talkEnd", FUNC(onDirectEnd)] call FUNC(addEventHandler);

// -- Because I don't trust talkEnd to always trigger okay, the PFH that updates the tags wil check if someone is still speaking instead -- //
/*["talkEnd", {
	(_this select 0) params ["_unit", "_nearby", "_name"];
	if !(_unit isEqualTo player) then {
		if ((side (group _unit)) isEqualTo playerSide) then {
			GVAR(speakerTags) deleteAt (GVAR(speakerTags) find [_name, _nearby]);
		};
	};
}] call FUNC(addEventHandler);
*/
