/*
 	Name: TFAR_fnc_sendPlayerKilled

 	Author(s):
		NKey

 	Description:
		Notifies the plugin that a unit has been killed.

	Parameters:
		OBJECT - Unit that was killed

 	Returns:
		Nothing

 	Example:
		player call FUNC(sendPlayerKilled);
*/

#include "macros.hpp"

_this setRandomLip false;
VAR(_request) = format["KILLED	%1", ([_this] call FUNC(name))];
VAR(_result) = PLUGIN_NAME callExtension _request;
