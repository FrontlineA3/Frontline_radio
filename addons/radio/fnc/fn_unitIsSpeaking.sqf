/*
    Function:       FRL_Radio_fnc_unitIsSpeaking
    Author:         Adanteh
    Description:    Checks if unit is speaking (Radio or Direct)
	Example:		[player] call FRL_Radio_fnc_unitIsSpeaking
*/
#include "macros.hpp"

params [["_unit", player]];
(_unit getVariable [QGVAR(speaking), false])
