/*
 *	File: fn_ServerInit.sqf
 *	Author: Adanteh
 *	Does radio init things on the server (Generate frequencies for side-specific channels)
 *  Object specific frequencies are generated on first-use only (No need to generate frequency for a vehicle that might never get used)
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FRL_fnc_ServerInit;
 */

#include "macros.hpp"

GVAR(frequencyBase) = 1;
GVAR(channel_name) = ["radio.channelname", "Taskforce Radio"] call FUNC(getConfig);
GVAR(channel_password) = ["radio.password", "lolnope"] call FUNC(getConfig);
GVAR(enforced) = ["radio.enforced", false] call FUNC(getConfig);
GVAR(ip) = ["radio.ip", "frontline-mod.com"] call FUNC(getConfig);

private _pubVars = [
	QGVAR(frequencyBase),
	QGVAR(channel_name),
	QGVAR(channel_password),
	QGVAR(enforced),
	QGVAR(ip)
];

// -- Generate a side and command channel for each side -- //
{
	_chSide 	= (format ["FRL_rSid%1", _x]);
	_chCommand 	= (format ["FRL_rCom%1", _x]);
	missionNamespace setVariable [_chSide, FREQ(2.5)];
	missionNamespace setVariable [_chCommand, FREQ(2.5)];
	_pubVars append [_chSide, _chCommand];
	nil;
} count [0, 1];

// -- Make these channels available globally -- //
{
	publicVariable _x;
	nil;
} count _pubVars;
