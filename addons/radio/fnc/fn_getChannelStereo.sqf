/*
    Function:       FRL_Radio_fnc_getChannelStereo
    Author:         Adanteh
    Description:    Gets stereo setting for given channel (Which ear to put sound on)
	Arguments:      0: The channel ID <SCALAR>
	Example:		[currentChannel] call FRL_Radio_fnc_getChannelStereo
*/

#include "macros.hpp"

params [["_channel", currentChannel]];
(missionNamespace getVariable [format [QGVAR(%1_stereo), _channel], 0]);

// -- 0: Stereo
// -- 1: Left
// -- 2: Right
// -- 3: Disabled
