/*
 *	File: fn_onDirectStart.sqf
 *	Author: Adanteh
 *	Shows the broadcasting hint when you're talking in local
 *
 *	0: Unit talking on direct <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[] call FUNC(onDirectStart);
 */

#include "macros.hpp"
#define __RADIOIMG "<img image='\A3\ui_f\data\igui\rscingameui\rscdisplayvoicechat\microphone_ca.paa'/>"

(_this select 0) params ["_unit", "_nearby", "_name"];

if (_unit isEqualTo TFAR_currentUnit) then {
	// -- Isn't talking on radio -- //
	if !(GVAR(isTalking)) then {
		// -- If error is currently showing, don't override the hint -- //
		if !(GVAR(lastError)) then {
			// -- Format structured text -- //
			VAR(_text) = format ["<t color='%2'>%3 %1 Communications</t>",
								([5] call FUNC(getChannelName)),
								([5] call FUNC(getChannelColor)),
								__RADIOIMG];
			[parseText _text, -1] call FUNC(radioHintShow);
		};
	};
};

true;