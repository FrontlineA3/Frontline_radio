/*
 	Name: TFAR_fnc_getTeamSpeakServerName

 	Author(s):
		NKey

 	Description:
		Returns TeamSpeak server name.

	Parameters:
		Nothing

 	Returns:
		STRING: name of server

 	Example:
		call FUNC(getTeamspeakServerName);
*/

#include "macros.hpp"

(PLUGIN_NAME callExtension "TS_INFO	SERVER")