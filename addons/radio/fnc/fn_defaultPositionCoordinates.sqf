/*
 	Name: TFAR_fnc_defaultPositionCoordinates

 	Author(s):
		NKey

 	Description:
		Prepares the position coordinates of the passed unit.

	Parameters:
		0: OBJECT - unit
		1: BOOLEAN - Is near player

 	Returns:
		Nothing

 	Example:


*/
#include "macros.hpp"

params ["_unit", ["_nearby", false], "_name", "_listenPos"];
private ["_relativePos", "_unitDirection"];

if (_unit != TFAR_currentUnit) then {
	_unitDirection = 0;
	if ((_unit getVariable ["frl_tempunit", false]) || !(alive _unit)) then {
		private _basePos = getMarkerPos (format ["base_%1", (side group _unit)]);
		_basePos set [2, (_basePos select 2) + 150];
		_relativePos = _basePos vectorDiff _listenPos;
	} else {
		// -- Simulate visiblePositionEyePos command -- //
		private _eyepos = if (_nearby) then {
			(visiblePosition _unit) vectorAdd ((eyePos _unit) vectorDiff (getPos _unit));
		} else {
			eyepos _unit;
		};
		_relativePos = _eyepos vectorDiff _listenPos;
	};
} else {
	_unitDirection = call FUNC(currentDirection);
	_relativePos = [0,0,0];
};

_relativePos pushBack _unitDirection;
_relativePos
