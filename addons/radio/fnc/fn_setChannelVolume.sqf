/*
    Function:       FRL_Radio_fnc_setChannelVolume
    Author:         Adanteh
    Description:    Sets the volume for the given channel
	Arguments:      0: The channel ID <Scalar>
                	1: The volume (0 to 10) <Scalar>
	Example:		[currentChannel] call FRL_Radio_fnc_setChannelVolume
*/
#include "macros.hpp"

params [["_channel", currentChannel], ["_volume", 7]];

_volume = (1 max _volume) min 10;
VAR(_varName) = format [QGVAR(%1_vol), _channel];
missionNamespace setVariable [_varName, _volume];
profileNamespace setVariable [_varName, _volume];
