/*
    Function:       FRL_Radio_fnc_getChannelListen
    Author:         Adanteh
    Description:    Gets all the channels a unit listens in on (Most of these are default, some are optional)
	Arguments:      0: The unit <OBJECT>
	Example:		[currentChannel, ] call FRL_Radio_fnc_getChannelListen
*/

#include "macros.hpp"

params [["_unit", player]];

private _channels = [/*_CH_SIDE, */_CH_GROUP];
if (leader _unit isEqualTo _unit) then { _channels pushBack _CH_COMMAND; }; // -- Only listen on command channel if you're a group leader -- //
if (missionNamespace getVariable [QEGVAR(Commander,isCommander), false]) then { _channels pushBackUnique _CH_COMMAND };
if (vehicle _unit != _unit) then { _channels pushBack _CH_VEHICLE; }; // -- Only listen to vehicle channel if you're actually in a vehicle -- //

// -- @todo: Add custom opt-in channels -- //

_channels;
