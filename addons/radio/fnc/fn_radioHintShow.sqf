/*
 *	File: fn_ShowHint.sqf
 *	Author: L-H		-Adjusted by Adanteh
 *	Displays a hint at the bottom left of the screen.
 *
 *	0: STRING - Text to display
 *	1: Number - Time to display the hint (-1 for infinite)
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
	["Hello", 3] call FUNC(radioHintShow);
	["Hello", -1] call FUNC(radioHintShow);
 */

#include "macros.hpp"
#define ctrl(x) (uiNamespace getVariable "frl_RadioHintDisplay" displayCtrl x)
#define disp (uiNamespace getVariable ["frl_RadioHintDisplay", displayNull])

params ["_text", ["_time", 3]/*, ["_sliderData", []]*/];

if (isNull disp) then {
	QGVAR(transmitLayer) cutRsc ["Rsc_FRL_RadioHint", "PLAIN", 0,true];
};

if (_text isEqualType "") then {
	_text = parseText _text;
};

ctrl(1) ctrlSetStructuredText _text;

// -- Add a slider to indicate things like volume. Hide by default -- //
/*if (_sliderData isEqualTo []) then {
	ctrl(2) ctrlShow false;
} else {
	ctrl(2) ctrlShow true;
	ctrl(2) sliderSetRange (_sliderData select 0);
	ctrl(2) sliderSetPosition (_sliderData select 1);
};
*/

if !(isNil QGVAR(hintHandle)) then {
	terminate GVAR(hintHandle);
};

if (_time == -1) exitWith {};

GVAR(hintHandle) = [_time] spawn {
	sleep (_this select 0);
	[] call FUNC(radioHintHide);
};

true;