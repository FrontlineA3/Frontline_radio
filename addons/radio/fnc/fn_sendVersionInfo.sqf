/*
 	Name: TFAR_fnc_sendVersionInfo

 	Author(s):
		NKey

 	Description:
		Sends information about the current TFAR version.

	Parameters:
		Nothing

 	Returns:
		Nothing

 	Example:
		call FRL_fnc_sendVersionInfo;
*/
// send information about version

#include "macros.hpp"

private ["_request","_result"];

// -- Does this move? Dunno bra -- //
_request = format["VERSION	%1	%2	%3", GVAR(version), GVAR(channel_name), GVAR(channel_password)];
_result = PLUGIN_NAME callExtension _request;