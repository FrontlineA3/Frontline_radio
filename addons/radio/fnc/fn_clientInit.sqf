/*
 *	File: fn_clientInit.sqf
 *	Author: Adanteh
 *	Runs init for radio on client
 *
 *	Example:
 *	[player] call FUNC(clientInit);
 */

#include "macros.hpp"

GVAR(isTalking) = false;
GVAR(voiceRangeFriendly) = 35;
GVAR(voiceRangeEnemy) = 4;
GVAR(voiceRangeEnemyFade) = 4;

// -- Nearby player detection -- //
GVAR(lastNearFrameTick) = diag_tickTime;
GVAR(lastFarFrameTick) = diag_tickTime;
GVAR(msPerStep) = 0;

GVAR(nearPlayers) = [];
GVAR(farPlayers) = [];

GVAR(nearPlayersIndex) = 0;
GVAR(nearPlayersProcessed) = true;

GVAR(farPlayersIndex) = 0;
GVAR(farPlayersProcessed) = true;

GVAR(msNearPerStepMax) = 0.025;
GVAR(msNearPerStepMin) = 0.1;
GVAR(msNearPerStep) = GVAR(msNearPerStepMax);
GVAR(nearUpdateTime) = 0.3;

GVAR(msFarPerStepMax) = 0.025;
GVAR(msFarPerStepMin) = 1.00;
GVAR(msFarPerStep) = GVAR(msFarPerStepMax);
GVAR(farUpdateTime) = 3.5;

GVAR(lastFrequencyInfoTick) = 0;
GVAR(lastNearPlayersUpdate) = 0;
GVAR(lastError) = false;

GVAR(msSpectatorPerStepMax) = 0.035;

// -- Get the server settings -- //
if (isNil QGVAR(enforced)) then {
    GVAR(enforced) = false;
};
[{
	// -- Init the keybinds -- //
	// -- Talk on currently selected channel (If invalid it broadcast over group) -- //
	["Frontline Radio","Radio_Transmit",["Radio Transmit","Transmit over radio on currently selected channel"],
		{ [currentChannel] call FUNC(onTransmitPressed); true }, {[currentChannel] call FUNC(onTransmitReleased);}, [0x3A, [nil, false, nil]], false, false] call FUNC(keybindAdd);

	// -- Transmit over command channel (If available) -- //
	["Frontline Radio","Command_Transmit",["Command Transmit","Transmit over radio over command channel (When available)"],
		{ [_CH_COMMAND] call FUNC(onTransmitPressed); true }, { [_CH_COMMAND] call FUNC(onTransmitReleased); }, [0x3A, [nil, true, nil]], false, false] call FUNC(keybindAdd);

    if ((alldisplays find (finddisplay 26) in [0,1]) || (alldisplays find (finddisplay 313) in [0,1])) exitWith { };
	if (isMultiplayer) then {
		call FUNC(sendVersionInfo);
		[FUNC(processPlayerPositions), 0] call FUNC(addPerFrameHandler);

		// -- Option to force radio usage -- //
		["init"] call FUNC(forceUsage);
	};

	true;
}, { !isNil QGVAR(channel_name) }] call FUNC(waitUntil);
