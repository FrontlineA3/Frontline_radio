/*
 	Name: TFAR_fnc_sendFrequencyInfo

 	Author(s):
		NKey, adjusted by Adanteh

 	Description:
		Notifies the plugin about the radios currently being used by the player and various settings active on the radio.
		Called every 0.5 seconds to update the extension with info on which channels we should receive from

	Parameters:
		Nothing

 	Returns:
		Nothing

 	Example:
		call FUNC(sendFrequencyInfo);
*/

#include "macros.hpp"

private _freq = [];

// -- Go through the channels a unit is currently listening through -- //
{
	_freq pushBack format ["%1%2|%3|%4",
		([_x] call FUNC(getChannelFrequency)),
		"_code",
		([_x] call FUNC(getChannelVolume)),
		([_x] call FUNC(getChannelStereo))];
	true;
} count ([TFAR_currentUnit] call FUNC(getChannelListen));

private _request = format["FREQ	%1	%2	%3	%4	%5	%6	%7	%8	%9	%10	%11	%12	%13",
	str(_freq),
	"[""No_LR_Radio""]", // _freq_lr
	"No_DD_Radio", // _freq_dd
	true, // _alive
	GVAR(voiceRangeFriendly),
	1, // GVAR(dd_volume_level)
	([TFAR_currentUnit] call FUNC(name)), // Player name
	waves,
	7, // GVAR(terrain_interception_coefficient)
	1,
	1,
	1.0, // _receivingDistanceMultiplicator
	20 // _speakerDistance
];
private _result = PLUGIN_NAME callExtension _request;
