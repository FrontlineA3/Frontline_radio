/*
 *	File: fn_onTransmitPressed.sqf
 *	Author: Adanteh
 *	On radio button press
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(onTransmitPressed);
 */

#include "macros.hpp"
#define __RADIOIMG "<img image='\A3\ui_f\data\igui\rscingameui\rscdisplayvoicechat\microphone_ca.paa'/>"

params [["_channel", currentChannel]];

if (!(GVAR(isTalking)) and {([TFAR_currentUnit] call FUNC(canSpeak))}) then {

	// -- Fallback to group channel if you can't talk on the current channel -- //
	if !(_channel in ([] call FUNC(getChannelTalk))) then {
		_channel = _CH_GROUP;
	};

	VAR(_channelName) = [_channel] call FUNC(getChannelName);
	VAR(_channelColor) = [_channel] call FUNC(getChannelColor);

	[format ["<t color='%2'>%3 %1 - Transmit</t>",
					_channelName,
					_channelColor,
					__RADIOIMG],

		format["TANGENT	PRESSED	%1%2	%3	%4", [_channel] call FUNC(getChannelFrequency), "_code", 50000, "digital"],
		-1
	] call FUNC(processTangent);
	GVAR(isTalking) = true;
};

true
