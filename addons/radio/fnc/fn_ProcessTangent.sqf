/*
 *	File: fn_ProcessTangent.sqf
 *	Author: Nkey, L-H		-Adjustd by Adanteh
 *	Process input to plugin and showing of radio hint when transmit starts/ends
 *
 *	0: Hint text (Structured) <STRING>
 *	1: Extension input <STRING>
 *	2: Time to show hint (Default: 2.5) <SCALAR>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[] call FUNC(ProcessTangent);
 */

#include "macros.hpp"

params ["_text", "_input", ["_hintTime", 2.5]];

// -- If error is currently showing, don't override the hint -- //
if !(GVAR(lastError)) then {
	// -- If no text is given hide the hitn -- //
	if (_text isEqualTo "") then {
		[] call FUNC(radioHintHide);
	} else {
		[parseText _text, _hintTime] call FUNC(radioHintShow);
	};
};

if (isMultiplayer) then {
	PLUGIN_NAME callExtension _input;
};

true;