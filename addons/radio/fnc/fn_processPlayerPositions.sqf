/*
 	Name: TFAR_fnc_processPlayerPositions

 	Author(s):
		NKey

 	Description:
		Process some player positions on each call and sends it to the plugin.

	Parameters:
		Nothing

 	Returns:
		Nothing

 	Example:
		call FUNC(processPlayerPositions);
*/
#include "macros.hpp"

if !(isNull (findDisplay 46)) then {
	if !(isNull TFAR_currentUnit) then {
		if ((GVAR(farPlayersProcessed)) and {GVAR(nearPlayersProcessed)}) then {
			GVAR(nearPlayersIndex) = 0;
			GVAR(farPlayersIndex) = 0;

			if (count GVAR(nearPlayers) == 0) then {
				GVAR(nearPlayers) = call FUNC(getNearPlayers);
			};

			VAR(_other_units) = +(allPlayers - GVAR(nearPlayers));

			GVAR(farPlayers) = [];
			GVAR(farPlayersIndex) = 0;
			{
				if !(_x getVariable [QGVAR(forceSpectator), false]) then {
					GVAR(farPlayers) set[GVAR(farPlayersIndex), _x];
					GVAR(farPlayersIndex) = GVAR(farPlayersIndex) + 1;
				};
				true;
			} count _other_units;

			GVAR(farPlayersIndex) = 0;

			if (count GVAR(nearPlayers) > 0) then {
				GVAR(nearPlayersProcessed) = false;
				GVAR(msNearPerStep) = GVAR(msNearPerStepMax) max (GVAR(nearUpdateTime) / (count GVAR(nearPlayers)));
				GVAR(msNearPerStep) = GVAR(msNearPerStep) min GVAR(msNearPerStepMin);
			} else {
				GVAR(msNearPerStep) = GVAR(nearUpdateTime);
			};
			if (count GVAR(farPlayers) > 0) then {
				GVAR(farPlayersProcessed) = false;
				if (count GVAR(nearPlayers) > 0) then {
					GVAR(msFarPerStep) = GVAR(msFarPerStepMax) max (GVAR(farUpdateTime) / (count GVAR(farPlayers)));
					GVAR(msFarPerStep) = GVAR(msFarPerStep) min GVAR(msFarPerStepMin);
				} else {
					GVAR(msFarPerStep) = GVAR(msSpectatorPerStepMax);
				};
			} else {
				GVAR(msFarPerStep) = GVAR(farUpdateTime);
			};
			call FUNC(sendVersionInfo);
		} else {
			private _listenPos = if (!(TFAR_currentUnit getVariable ["frl_tempUnit", false]) && alive TFAR_currentUnit) then {
				eyePos TFAR_currentUnit;
			} else {
				private _basePos = (getMarkerPos (format ["base_%1", playerSide]));
				_basePos set [2, (_basePos select 2) + 150];
				_basePos;
			};

			VAR(_elemsNearToProcess) = (diag_tickTime - GVAR(lastNearFrameTick)) / GVAR(msNearPerStep);
			if (_elemsNearToProcess >= 1) then {
				for "_y" from 0 to _elemsNearToProcess step 1 do {
					if (GVAR(nearPlayersIndex) < count GVAR(nearPlayers)) then {
						VAR(_unit) = (GVAR(nearPlayers) select GVAR(nearPlayersIndex));
						[_unit, true, ([_unit] call FUNC(name)), _listenPos] call FUNC(sendPlayerInfo);
						GVAR(nearPlayersIndex) = GVAR(nearPlayersIndex) + 1;
					} else {
						GVAR(nearPlayersIndex) = 0;
						GVAR(nearPlayersProcessed) = true;

						if (diag_tickTime - GVAR(lastNearPlayersUpdate) > 0.5) then {
							GVAR(nearPlayers) = call FUNC(getNearPlayers);
							GVAR(lastNearPlayersUpdate) = diag_tickTime;
						};
					};
				};
				GVAR(lastNearFrameTick) = diag_tickTime;
			};

			VAR(_elemsFarToProcess) = (diag_tickTime - GVAR(lastFarFrameTick)) / GVAR(msFarPerStep);
			if (_elemsFarToProcess >= 1) then {
				for "_y" from 0 to _elemsFarToProcess step 1 do {
					if (GVAR(farPlayersIndex) < count GVAR(farPlayers)) then {
						VAR(_unit) = (GVAR(farPlayers) select GVAR(farPlayersIndex));
						[_unit, false, ([_unit] call FUNC(name)), _listenPos] call FUNC(sendPlayerInfo);
						GVAR(farPlayersIndex) = GVAR(farPlayersIndex) + 1;
					} else {
						GVAR(farPlayersIndex) = 0;
						GVAR(farPlayersProcessed) = true;
					};
				};
				GVAR(lastFarFrameTick) = diag_tickTime;
			};
		};
		if (diag_tickTime - GVAR(lastFrequencyInfoTick) > 0.5) then {
			call FUNC(sendFrequencyInfo);
			GVAR(lastFrequencyInfoTick) = diag_tickTime;
		};
	};
};
