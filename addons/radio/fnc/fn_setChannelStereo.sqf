/*
    Function:       FRL_Radio_fnc_setChannelStereo
    Author:         Adanteh
    Description:    Sets the stereo mode for given channel (Which side to give sound on)
	Arguments:      0: The channel ID <Scalar>
                	1: The volume (0 to 3) <Scalar>
	Example:		[currentChannel] call FRL_Radio_fnc_setChannelStereo
*/
#include "macros.hpp"

params [["_channel", currentChannel], ["_stereoMode", 0]];

VAR(_varName) = format [QGVAR(%1_stereo), _channel];
missionNamespace setVariable [_varName, _stereoMode];
profileNamespace setVariable [_varName, _stereoMode];

// -- 0: Stereo
// -- 1: Left
// -- 2: Right
// -- 3: Disabled

true;
