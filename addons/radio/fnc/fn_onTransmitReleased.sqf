/*
 *	File: fn_onTransmitReleased.sqf
 *	Author: Adanteh
 *	On radio button release
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(onTransmitReleased);
 */

#include "macros.hpp"

params [["_channel", currentChannel]];

if (GVAR(isTalking)) then {
	["",
		format["TANGENT	RELEASED	%1%2	%3	%4",
		([_channel] call FUNC(getChannelFrequency)),
		"_code",
		50000,
		"digital"]
	] call FUNC(processTangent);

	GVAR(isTalking) = false;
};
true
