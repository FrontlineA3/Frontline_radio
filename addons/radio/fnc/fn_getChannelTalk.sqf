/*
    Function:       FRL_Radio_fnc_getChannelTalk
    Author:         Adanteh
    Description:    Checks if given channel is a channel we can talk on
    Arguments:      0: The unit <OBJECT>
    Example:		[currentChannel] call FRL_Radio_fnc_getChannelTalk
*/

#include "macros.hpp"

params [["_unit", TFAR_currentUnit]];

private _channels = [_CH_GROUP];
if (leader _unit isEqualTo _unit) then { _channels pushBack _CH_COMMAND; }; // -- Only talk on command channel if you're a group leader -- //
if (missionNamespace getVariable [QEGVAR(Commander,isCommander), false]) then { _channels pushBackUnique _CH_COMMAND };
if (vehicle _unit != _unit) then { _channels pushBack _CH_VEHICLE; }; // -- Only talk to vehicle channel if you're actually in a vehicle -- //

// -- @todo: Add custom opt-in channels -- //
// -- @todo: Auto-detect support vehicles (CAS, Armor, Mortar) and listen to Support channel -- //

_channels;
