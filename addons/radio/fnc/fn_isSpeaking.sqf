/*
 	Name: TFAR_fnc_isSpeaking

 	Author(s):
		L-H

 	Description:
		Check whether a unit is speaking

	Parameters:
		OBJECT - Unit

 	Returns:
		BOOLEAN - If the unit is speaking

 	Example:
		if (player call FUNC(isSpeaking)) then {
			hint "You are speaking";
		};
*/

#include "macros.hpp"

((PLUGIN_NAME callExtension format ["IS_SPEAKING	%1", ([_this] call FUNC(name))]) == "SPEAKING")
