/*
    Function:       FRL_Radio_fnc_getChannelName
    Author:         Adanteh
    Description:    Gets display name of a channel
	Arguments:      0: The channel ID <SCALAR>
	Example:		[currentChannel] call FRL_Radio_fnc_getChannelName
*/

#include "macros.hpp"

params [["_channel", currentChannel]];

VAR(_channelName) = ["Global", "Side", "Command", "Group", "Vehicle", "Direct"] param [_channel, ""];
if (_channelName isEqualTo "") then {
	// -- If channel name is not found, check if there's a custom name (Custom channels have a +5 offset) -- //
	VAR(_channelName) = (missionNamespace getVariable ["FRL_cNames", []]) param [(_channel - 5), ""];
};

_channelName
