/*
 	Name: TFAR_fnc_preparePositionCoordinates

 	Author(s):
		NKey

 	Description:
		Prepares the position coordinates of the passed unit.

	Parameters:
		0: OBJECT - unit
		1: BOOLEAN - Is near player
		3: STRING - Unit name

 	Returns:
		Nothing

 	Example:
 	[testObject, true, "Leon"] call FUNC(preparePositionCoordinates);

*/
#include "macros.hpp"

params ["_unit", "_nearPlayer", "_name", "_listenPos"];

VAR(_pos) = _this call FUNC(defaultPositionCoordinates);
VAR(_isolated_and_inside) = _unit call FUNC(vehicleIsIsolatedAndInside);
VAR(_can_speak) = [_unit] call FUNC(canSpeak);
VAR(_vehicle) = _unit call FUNC(vehicleId);

private _volume = 1;
if !(_nearPlayer) then {
	if (side group _unit != playerSide) then {
		_volume = (1 min (1 - (((_unit distance TFAR_currentUnit) - GVAR(voiceRangeEnemy)) /  GVAR(voiceRangeEnemyFade)))) max 0;
	};
};

(format["POS	%1	%2	%3	%4	%5	%6	%7	%8	%9	%10	%11	%12	%13",
	_this select 2,
	_pos select 0,
	_pos select 1,
	_pos select 2,
	_pos select 3,
	_can_speak,
	true, // Use SW
	false, // Use LR
	false, // Use DD
	_vehicle,
	0, // Terraininterception
	_volume,
	call FUNC(currentDirection)
])