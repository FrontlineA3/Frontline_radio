/*
    Function:       FRL_Radio_fnc_getNearPlayers
    Author:         N-Key, Adanteh
    Description:    Gets nearby units that you can hear on direct comms (Includes vehicle crew)
					This has a lower range for non-friendly units
    Example:        call FRL_Radio_fnc_getNearPlayers;
*/

#include "macros.hpp"

private _result = [];
if !(alive TFAR_currentUnit) then {
	private _allUnits = nearestObjects [TFAR_currentUnit, ["CaManBase"], GVAR(voiceRangeFriendly)];
	if (count (units (group TFAR_currentUnit)) < 10) then {
		{
			if (_x != TFAR_currentUnit) then {
				_allUnits pushBack _x;
			};
			nil;
		} count (units (group TFAR_currentUnit));
	};

	{
		_allUnits append (crew _x);
		nil;
	} count (TFAR_currentUnit nearEntities [["LandVehicle", "Air", "Ship"], GVAR(voiceRangeFriendly)]);

	private _playerSide = playerSide;
	{
		if ((isPlayer _x) && (alive _x)) then {
			if (side group _x == _playerSide || {(TFAR_currentUnit distance _x) <= GVAR(voiceRangeEnemy)}) then {
				_result pushBack _x;
			};
		};
		nil;
	} count _allUnits;
};
_result
