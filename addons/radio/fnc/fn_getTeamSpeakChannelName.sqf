/*
 	Name: TFAR_fnc_getTeamSpeakChannelName

 	Author(s):
		NKey

 	Description:
		Returns TeamSpeak channel name.

	Parameters:
		Nothing

 	Returns:
		STRING: name of channel

 	Example:
		call FUNC(getTeamspeakChannelName);
*/

#include "macros.hpp"

(PLUGIN_NAME callExtension "TS_INFO	CHANNEL")