/*
    Function:       FRL_Radio_fnc_getChannelTalk
    Author:         Adanteh
    Description:    Gets the volume for the current channel
	Arguments:      0: The channel ID <Scalar>
	Example:		[currentChannel] call FRL_Radio_fnc_getChannelTalk
*/
#include "macros.hpp"

params [["_channel", currentChannel]];
(missionNamespace getVariable [format [QGVAR(%1_vol), _channel], 7]);
