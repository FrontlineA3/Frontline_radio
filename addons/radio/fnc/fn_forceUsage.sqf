/*
    Function:       FRL_Radio_fnc_forceUsage
    Author:         Adanteh
    Description:    Function to force usage of radio mod. Makes sure player is in correct channel, on correct server and has the plugin running
	Example:		["init"] call FRL_Radio_fnc_forceUsage
*/

#include "macros.hpp"

params ["_mode", ["_args", []]];
private _return = false;
switch (toLower _mode) do {
	case "init": { // -- Check if plugin is enabled and connected to the correct server and channel every 30 seconds -- //
		if (GVAR(enforced) && {!((alldisplays find (finddisplay 26) in [0,1]) || (alldisplays find (finddisplay 313) in [0,1]))}) then {

			// -- Add a small delay before checking, so the plugin has some time to move us -- //
			[{
				[{
					if (GVAR(enforced)) then {
						if (missionNamespace getVariable [QGVAR(alreadyChecking), false]) exitWith { };
						["check"] call FUNC(forceUsage);
					};
				}, 30, []] call FUNC(addPerFrameHandler);
			}, 10] call FUNC(wait);
		};
	};

	// -- Check the version of client and compare it to server -- //
	case "check": {
		_args params [["_recheck", false]];
		private _currentChannel = [] call FUNC(getTeamspeakChannelName);
		if !(_currentChannel isEqualTo GVAR(channel_name)) then {
			["screen", [GVAR(channel_name), GVAR(channel_password), _recheck]] call FUNC(forceUsage);
			_return = false;
		} else {
			_return = true;
		};
	};

	case "screen": {
		// -- Don't create double screen -- //
		_args params ["_channelRequired", "", "_recheck"];
		private _teamspeakIP = GVAR(ip);
		private _msg = format ["<t>You need to have the radio mod connected to Teamspeak to play on this server. Make sure the TFAR plugin is enabled as well (Install the .ts3_plugin in the @Frontline folder!).<br/><br/>" +
        "Teamspeak IP:   <a href='#'>%1</a><br/>Channel:   <a href='#'>%2</a><br/><br/>" +
        "If you are connected and have the plugin enabled, but aren't moved, reload it (In teamspeak Tools>Options>Addons>Reload All)<br /><br />" +
        "This screen will automatically close once you are connected. <br/><br/>"+
        "Alternatively just use the <a href='http://get.frontline-mod.com'>launcher</a> so you don't need to worry about any of this stuff!</t>", _teamspeakIP, _channelRequired];
		if (_recheck) then { _msg = format ["-- Rechecking failed. Still not connected [%2] --<br/><br/>%1", _msg, round time]; };

		GVAR(alreadyChecking) = true;
		["radiorequired", true, _msg] call frl_main_fnc_blockingMessage;
		[{
			if (["check", [true]] call FUNC(forceUsage) || !GVAR(enforced)) then {
				["radiorequired", false] call frl_main_fnc_blockingMessage;
				GVAR(alreadyChecking) = false;
			};
		}, 5] call FUNC(wait);
	};
};

_return;
