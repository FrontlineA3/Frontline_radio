/*
 	Name: TFAR_fnc_getTeamSpeakPluginEnabled

 	Author(s):
		NKey

 	Description:
		Returns is TeamSpeak plugin enabled on client.

	Parameters:
		Nothing

 	Returns:
		BOOLEAN: enabled or not

 	Example:
		call FUNC(getTeamspeakPluginEnabled);
*/

#include "macros.hpp"

(PLUGIN_NAME callExtension "TS_INFO	PING") == "PONG"