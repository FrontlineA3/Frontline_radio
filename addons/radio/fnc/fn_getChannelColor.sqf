/*
    Function:       FRL_Radio_fnc_getChannelColor
    Author:         Adanteh
    Description:    Gets channel text color (HTML RGBA format)
	Arguments:      0: The channel ID <SCALAR>
	Example:		[currentChannel] call FRL_Radio_fnc_getChannelColor
*/
#include "macros.hpp"

params [["_channel", currentChannel]];
(GVAR(channelColors) param [_channel, "#FFFFFFFF"]);
