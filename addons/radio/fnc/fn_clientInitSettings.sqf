/*
 *	File: fn_clientInitSettings.sqf
 *	Author: Adanteh
 *	Inits the radio settings for attributes window. Allows changing things like volume level and stereo mode.
 *	These settings are all profile bound, so you can actually use them without constantly resetting
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(clientInitSettings);
 */

#include "macros.hpp"

if (isNil "FRL_Main_fnc_settingsWindow") exitWith { };

// -- Load settings from the profile on mission start and add settings to attributes window -- //
{
	private _channelName = ([_x] call FUNC(getChannelName));
	private _volumeProfile = profileNamespace getVariable [format [QGVAR(%1_vol), _x], 7];
	private _stereoProfile = profileNamespace getVariable [format [QGVAR(%1_stereo), _x], 0];
	missionNamespace setVariable [format [QGVAR(%1_vol), _x], _volumeProfile];
	missionNamespace setVariable [format [QGVAR(%1_stereo), _x], _stereoProfile];
	// -- Volume levels -- //
	['add', ["gui", 'Frontline Settings', "Radio Settings", [
		format ['%1 - Volume', _channelName],
		'Volume Level',
		"FRL_main_CtrlSlider",
		(format ["[%1] call %2;", _x, QFUNC(getChannelVolume)]),
		{ true },
		(format ["[%1, (_this select 1)] call %2; false", _x, QFUNC(setChannelVolume)]),
		[[1, 10], [0.05, 1]]
	]]] call FRL_Main_fnc_settingsWindow;

	// -- Which audio channel -- //
	['add', ["gui", 'Frontline Settings', "Radio Settings", [
		'Audio Channel',
		'Controls which audio channel you hear the sound on (Disabled because no effect yet)',
		"FRL_main_ctrltoolbox3",
		(format ["[%1] call %2;", _x, QFUNC(getChannelStereo)]),
		{ true }, // -- Not functional right now
		(format ["[%1, (_this select 0) lbValue (_this select 1)] call %2; false", _x, QFUNC(setChannelStereo)]),
		[
			["Left Mono", 1],
			["Stereo", 0],
			["Right Mono", 2]
		]
	]]] call FRL_Main_fnc_settingsWindow;

	// --  Add spacer between channels
	if (_forEachIndex < 2) then {
		['add', ["gui", 'Frontline Settings', "Radio Settings", [
			'',
			'',
			"FRL_main_ctrlSetting"
		]]] call FRL_Main_fnc_settingsWindow;
	};
} forEach [_CH_GROUP, _CH_COMMAND, _CH_VEHICLE];
