/*
 *	File: fn_radioHintHide.sqf
 *	Author: Adanteh
 *	Removes radio hint (Bottom left)
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[] call FUNC(radioHintHide);
 */

#include "macros.hpp"

QGVAR(transmitLayer) cutText ["", "PLAIN"];

true;