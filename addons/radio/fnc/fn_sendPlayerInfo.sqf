/*
 	Name: FUNC(sendPlayerInfo)

 	Author(s):
		NKey

 	Description:
		Notifies the plugin about a player

	Parameters:
		0: OBJECT - Unit
		1: BOOLEAN - Is unit close to player
		2: STRING - Unit name


 	Returns:
		Nothing

 	Example:
		[cursorTarget, true, ([cursorTarget] call FUNC(name))] call FUNC(sendPlayerInfo);
*/
#include "macros.hpp"

params ["_unit", "_nearby", "_name", "_listenPos"];

private _request = _this call FUNC(preparePositionCoordinates);
private _result = PLUGIN_NAME callExtension _request;

if ((_result != "OK") and {_result != "SPEAKING"} and {_result != "NOT_SPEAKING"}) then {
	[parseText (_result), 10] call FUNC(radioHintShow);
	GVAR(lastError) = true;
} else {
	if (GVAR(lastError)) then {
		call FUNC(radioHintHide);
		GVAR(lastError) = false;
	};
};

if (_result == "SPEAKING") then {
	_unit setRandomLip true;
	if (!(_unit getVariable [QGVAR(speaking), false])) then {
		_unit setVariable [QGVAR(speaking), true];
		["talkStart", [_unit, _nearby, _name]] call FUNC(localEvent);
	};
	_unit setVariable [QGVAR(start_speaking), diag_tickTime];
} else {
	_unit setRandomLip false;
	if ((_unit getVariable [QGVAR(speaking), false])) then {
		_unit setVariable [QGVAR(speaking), false];
		["talkEnd", [_unit, _nearby, _name]] call FUNC(localEvent);
	};
};
