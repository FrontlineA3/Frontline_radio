#define DOUBLE(var1,var2) var1##_##var2
#define TRIPLE(var1,var2,var3) DOUBLE(var1,DOUBLE(var2,var3))
#define QUOTE(var) #var

// -- Vars -- //
#define VAR(var1) private var1
#define EGVAR(var1,var2) TRIPLE(PREFIX,var1,var2)
#define QEGVAR(var1,var2) QUOTE(EGVAR(var1,var2))

#define GVAR(var1) EGVAR(MODULE,var1)
#define QGVAR(var1) QEGVAR(MODULE,var1)

// -- Short variables (No module name, use for easy subclasses)
#define SVAR(var1) DOUBLE(PREFIX,var1)
#define QSVAR(var1) QUOTE(SVAR(var1))

// -- Functions -- //
#define EFUNC(var1,var2) TRIPLE(PREFIX,var1,DOUBLE(fnc,var2))
#define QEFUNC(var1,var2) QUOTE(EFUNC(var1,var2))

#define FUNC(var1) EFUNC(MODULE,var1)
#define QFUNC(var1) QUOTE(FUNC(var1))
#define DFUNC(var1) EFUNC(MODULE,var1)

// -- Configs -- //
#define VERSION MAJOR.MINOR.PATCHLVL.BUILD
#define VERSION_AR MAJOR,MINOR,PATCHLVL,BUILD
#define ADDON DOUBLE(PREFIX,MODULE)
