#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class CfgMods {
    class Mod_Base;
    class ADDON : Mod_Base {
        name = "Frontline Radio";
        picture = "\pr\frl\addons\main\ui\frontline_logo_256.paa";
        logo = "\pr\frl\addons\main\ui\frontline_logo_64.paa";
        logoOver = "\pr\frl\addons\main\ui\frontline_logo_64.paa";
        logoSmall = "\pr\frl\addons\main\ui\frontline_logo_32.paa";

        tooltip = "Frontline Radio";
        tooltipOwned = "Frontline Radio";
        overview = "Frontline Radio, a TFAR modification.";
        overviewPicture = "\pr\frl\addons\main\ui\frontline_logo_64.paa";

        hideName = 0;
        hidePicture = 0;

        actionName = "Website";
        action = "http://www.frontline-mod.com";
        description = "Frontline Radio";
        author = "Adanteh & [TF]Nkey";
    };
};

class RscStructuredText {
    class Attributes;
};

class RscTitles {
    #include "ui\RscSpeakerTags.hpp"
    #include "ui\RscTaskForceHint.hpp"
};

class FRL {
    class Modules {
        class Radio {
            dependencies[] = {"Main"};
            path = "\pr\FRL\addons\radio\fnc";
            class initRadio;
            class clientInit;
            class clientInitUI;
            class clientInitSettings;

            class canSpeak;
            class currentDirection;
            class defaultPositionCoordinates;
            class forceUsage;
            class getChannelFrequency;
            class getChannelListen;
            class getChannelStereo;
            class getChannelTalk;
            class getChannelVolume;
            class getChannelColor;
            class getChannelName;
            class getNearPlayers;
            class getTeamspeakChannelName;
            class getTeamspeakPluginEnabled;
            class getTeamspeakServerName;
            class radioHintHide;
            class isSpeaking;
            class onDirectStart;
            class onDirectEnd;
            class onTransmitPressed;
            class onTransmitReleased;
            class preparePositionCoordinates;
            class processPlayerPositions;
            class processTangent;
            class sendFrequencyInfo;
            class sendPlayerInfo;
            class sendPlayerKilled;
            class sendVersionInfo;
            class serverInit;
            class setChannelVolume;
            class setChannelStereo;
            class radioHintShow;
            class vehicleID;
            class vehicleIsIsolatedAndInside;
            class unitIsSpeaking;

            class Wrapper {
              path = "\pr\FRL\addons\radio\wrapper";
              class addEventHandler;
              class addPerFrameHandler;
              class keybindAdd;
              class keybindCheck;
              class localEvent;
              class name;
              class wait;
              class waitUntil;
              class getConfig;
            }
        };
    };
};
