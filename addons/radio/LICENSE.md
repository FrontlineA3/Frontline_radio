### LICENSE ###

This is based on the TFAR radio mod [TF]Nkey, originally released under APL-SA License ![](https://www.bistudio.com/assets/img/licenses/APL-SA.png) 
Full License: https://www.bistudio.com/community/licenses/arma-public-license-share-alike

Current code can be found here https://github.com/michail-nikolaev/task-force-arma-3-radio

The modified Frontline version is released under the same APL-SA License. This only applies to the FRL_Radio module
![](https://www.bistudio.com/assets/img/licenses/APL-SA.png) 
Full License: https://www.bistudio.com/community/licenses/arma-public-license-share-alike


