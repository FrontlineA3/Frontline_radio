// -- Module defines -- //
#define MODULE Radio
#define PLUGIN_NAME "frontline_radio"

#define TFAR_currentUnit Clib_Player

#define _CH_GLOBAL 0
#define _CH_SIDE 1
#define _CH_COMMAND 2
#define _CH_GROUP 3
#define _CH_VEHICLE 4
#define _CH_DIRECT 5
#define FREQ(x) (if true then { GVAR(frequencyBase) = GVAR(frequencyBase) + x; str GVAR(frequencyBase) })

// -- Global defines -- //
#include "modinfo.hpp"
#include "macros_base.hpp"
