#define PATH pr
#define PREFIX FRL
#define SUBPREFIX addons
#define FUNCTIONPREFIX FRL

#define AUTHOR_STR "Adanteh"
#define AUTHOR_ARR {"Adanteh"}
#define AUTHOR_URL "http://www.frontline-mod.com"

#define REQUIRED_VERSION 1.64

#define MAJOR 0
#define MINOR 1
#define PATCHLVL 1
#define BUILD 1

#define VERSION MAJOR.MINOR.PATCHLVL.BUILD
#define VERSION_AR MAJOR,MINOR,PATCHLVL,BUILD
