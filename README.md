# Frontline Radio #

### What is this? ###
This is a heavily modified version of the Task Force Radio mod by [TF]Nkey and Dedmen. In short it cuts out all 'radio' items and replaces it by a system based on the vanilla channels. (Group for group communiations, Command if you're an SL, Vehicle if in a vehicle and so on). It's not meant to be realistic or suitable for everyone and the reason it exists is because in-game VoN is still laggy and not moddable.

The TFAR Repository can be find [here](https://github.com/michail-nikolaev/task-force-arma-3-radio)

### How it works ###
* To keep compatible with TFAR plugin, a frequency is created for each channel
* Every 0.5s the frequencies of the channels available to user are send to plugin, so it knows which frequencies to listen in on
* When talking, the frequency of the current channel is retrieved and a broadcast is made on this frequency
* Outside of the vanilla channels an additional 'Support' channel is avaiable, suitable for Fire support needs (Armor, CAS, Mortar).


### LICENSE ###
This is based on the TFAR radio mod by [TF]Nkey, originally released under APL-SA License ![](https://www.bistudio.com/assets/img/licenses/APL-SA.png)
Full License: https://www.bistudio.com/community/licenses/arma-public-license-share-alike

Current code can be found here https://github.com/michail-nikolaev/task-force-arma-3-radio
